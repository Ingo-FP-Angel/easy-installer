:: Copyright (C) 2020 - Author: Ingo
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: TODO: What if 2 devices detected?
:: $1: The folder where fastboot runnable is stored

:: Exit status
:: - 0 : Device in fastboot mode detected
:: - 1 : Error

@echo Off

set FASTBOOT_FOLDER_PATH=%~1
set ADB_PATH="%FASTBOOT_FOLDER_PATH%adb"
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"


:fastboot_detect
%FASTBOOT_PATH% oem device-info 2>&1 | findstr /c:"critical unlocked: true"
if errorLevel 1 (
	waitfor /t 2 pause 2>nul
	goto :fastboot_detect
) 

call fastboot_detect

:: waitfor /t 5 pause 2>nul
echo "fastboot mode detected, phone is critically unlocked"

exit /b 0

	
	