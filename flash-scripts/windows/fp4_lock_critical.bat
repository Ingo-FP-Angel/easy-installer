:: Copyright (C) 2020 - Author: Ingo; update and adoption to FP4, 2022 by ff2u
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: The folder where fastboot runnable is stored
:: $2: The archive folder 
:: $3: The device model

:: Exit status
:: - 0 : success
:: - 1 : Error
:: - 101 : locking the bootloader failed

@echo OFF

set FASTBOOT_FOLDER_PATH=%~1
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"
echo "FASTBOOT path:"%FASTBOOT_PATH%


::-----------
set ARCHIVE_PATH=%~2
echo "Archive Path=%ARCHIVE_PATH%"
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH="%%~dpa
)

echo "Archive Folder Path="%ARCHIVE_FOLDER_PATH%

set device_model=%~3

echo "Model=%device_model%"

set SECURITY_PATCH=%ARCHIVE_FOLDER_PATH%%device_model%-security-patch"
set /p ORIGINAL_SECURITY_PATCH=<%SECURITY_PATCH%
call:versionToInt %ORIGINAL_SECURITY_PATCH%
set I_ORIGINAL_SECURITY_PATCH=%var1%
echo "I_ORIGINAL_SECURITY_PATCH====>%I_ORIGINAL_SECURITY_PATCH%"

set MURENA__SECURITY_PATCH=
set MURENA_ROM_INFO=%ARCHIVE_FOLDER_PATH%%device_model%-rom-info"
for /f %%i in ('findstr /b "ro.build.version.security_patch"  %MURENA_ROM_INFO%') do (
   set MURENA__SECURITY_PATCH=%%i
 )
SET MURENA__SECURITY_PATCH=%MURENA__SECURITY_PATCH:~32%
echo "MURENA__SECURITY_PATCH=%MURENA__SECURITY_PATCH%"
call:versionToInt %MURENA__SECURITY_PATCH%
set I_MURENA__SECURITY_PATCH=%var1%
echo "I_MURENA__SECURITY_PATCH====>%I_MURENA__SECURITY_PATCH%"

if %I_ORIGINAL_SECURITY_PATCH% LSS 1 (
    echo "ORIGINAL ROM INFO NOT AVAILABLE => DO NOT PROCESS"
    exit /b 0
)

if %I_MURENA__SECURITY_PATCH% GEQ %I_ORIGINAL_SECURITY_PATCH% (
    echo "GREATER OR EQUALS OR ORIGINAL ROM INFO NOT AVAILABLE => PROCESS"
) else (
	echo "LOWER DO NOT PROCESS"
    exit /b 0
)
:: ------------------

%FASTBOOT_PATH% flashing lock_critical

if errorLevel 1 (
	echo "Lock Critical fails!"
        exit /b 101
)

:wait-leave-fastboot
%FASTBOOT_PATH% devices 2>nul | findstr fastboot
if %ERRORLEVEL% EQU 0 (
    waitfor /t 1 pause 2>nul
    goto :wait-leave-fastboot 
)

exit /b 0



:: Assuming format is xxxx-yy-zz with otional will return xxyyzz
:versionToInt 
	setlocal enabledelayedexpansion
	set str_sum=
	set version=%~1
	set version=%version:-= %
	
	for %%a in (%version%) do (
		set "formattedValue=000000%%a"
		set str_sum=!str_sum!!formattedValue:~-2!
	)

	( endlocal 
		set "var1=%str_sum%"
	)
	goto:eof