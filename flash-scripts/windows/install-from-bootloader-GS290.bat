#!/bin/bash

# Copyright (C) 2020 ECORP SAS - Author: Romain Hunault
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: DEVICE_ID device id
# $2: ARCHIVE_PATH path to archive
# $3: fastboot folder path

# Exit status
# - 0 : device flashed
# - 1 : generic error
# - 101 : DEVICE_ID missing
# - 102 : ARCHIVE_PATH missing
# - 103 : fastboot folder path missing


set DEVICE_ID="%1"
set ARCHIVE_PATH=%~2
set FASTBOOT_FOLDER_PATH=%~3
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

if not defined %FASTBOOT_FOLDER_PATH (
  exit /b 103
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_NAME="%%~na"
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH="%%~dpa"
)

echo "archive path : "%ARCHIVE_PATH%
echo "archive folder path : "%ARCHIVE_FOLDER_PATH%
echo "fastboot path : "%FASTBOOT_PATH%

cd "%ARCHIVE_FOLDER_PATH%"

timeout 1 >nul 2>&1

# Should we unzip ?

%FASTBOOT_PATH% -s %DEVICE_ID% erase userdata
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% flash --disable-verity --disable-verification boot boot.img
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% flash recovery recovery-e-latest-GS290.img
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% flash system system.img
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% flash lk lk.img
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% flash logo logo.bin
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% -w
sleep 1
%FASTBOOT_PATH% -s %DEVICE_ID% reboot
sleep 1
