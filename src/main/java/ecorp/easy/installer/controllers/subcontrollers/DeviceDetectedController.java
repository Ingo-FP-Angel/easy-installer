/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;


import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.helpers.DeviceHelper;
import ecorp.easy.installer.exceptions.TooManyDevicesException;
import ecorp.easy.installer.models.Phone;
import ecorp.easy.installer.tasks.DeviceDetectionTask;
import ecorp.easy.installer.utils.UiUtils;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ResourceBundle;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Vincent Bourgmayer
 */
public class DeviceDetectedController extends AbstractSubController{
    private final static Logger logger = LoggerFactory.getLogger(DeviceDetectedController.class);
    @FXML private Button buyNewDevice;
    @FXML private Button startDetectionButton;
    @FXML private Label detectionMsg;
    @FXML private VBox deviceDetectedRoot;
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);
        UiUtils.hideNode(buyNewDevice);
        UiUtils.hideNode(startDetectionButton);
        detectionMsg.setText(i18n.getString("detect_lbl_detecting"));
        startDetection();
    }        
    
    /**
     * Start the process of detection
     */
    public void startDetection(){
        logger.info("startDetection()");
        
        if(deviceDetectedRoot.getChildren().get(0).getClass().equals(VBox.class)){
            deviceDetectedRoot.getChildren().remove(0);
            UiUtils.showNode(detectionMsg);
        }
        
        UiUtils.hideNode(buyNewDevice);
        UiUtils.hideNode(startDetectionButton);

        String labelMsg = i18n.getString("detect_lbl_detecting");

        detectionMsg.setText(labelMsg);
        DeviceDetectionTask detectorTask = new DeviceDetectionTask();
        detectorTask.setOnSucceeded((WorkerStateEvent t) -> {
            Phone result = detectorTask.getValue();
            handleDetectionResult(result);
        });
        
        detectorTask.setOnFailed((WorkerStateEvent t) -> {
            if(detectorTask.getException() instanceof TooManyDevicesException){
                detectionMsg.setText(i18n.getString("detect_lbl_tooManyDevicesDetected"));
                showTryAgainButton(true);
            }

        });

        Thread thread = new Thread(detectorTask);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Handle result of the DeviceDetectionTask
     * @param phone 
     */
    private void handleDetectionResult(Phone phone){

        //If result is null, then retry
        if (phone == null ){ 
            startDetection();
            return; 
        }

        //If device is unauthorized
        if(!phone.isAdbAuthorized()){
            displayUnauthorizedDeviceFound();
            return;
        }

        if (DeviceHelper.isDeviceBlacklisted(phone.getAdbDevice())) {
            displayIncompatibleDeviceFound(phone.getAdbModel());
            return;
        }

        
        final String model = phone.getAdbModel();
        if(model == null || model.isEmpty()) displayUnknowDeviceFound();
        else{
            //check that there is config file for this device
            detectionMsg.setText(String.format(i18n.getString("detect_lbl_compatibleDeviceFound"), model));
            if(parentController != null){
                
                // Fill last property of Phone object
                phone.setInternalCode(DeviceHelper.getDeviceInternalcode(phone.getAdbDevice()));

                try{
                    phone.setFlashingProcess(DeviceHelper.loadFlashProcess(phone.getAdbDevice()));
                }catch(IOException | NullPointerException | NumberFormatException | ParseException e){
                    logger.debug("Can't load Flashing Process from Yaml : {}", e.getMessage());
                    displayIncompatibleDeviceFound(model);
                    return;
                }

                AppConstants.setDeviceModel(phone.getAdbDevice()); //this line must be before the "parentController.setDevice(phone). If it's not the case,

                //the AppConstants.getSourceFolder() won't include the device model's name
                parentController.setPhone(phone);
                parentController.disableNextButton(false);
            }
        }
    }

    
    //Note: Two below function could be merged and use a parameter to define detectionMsg.setText..
    /**
     * Update UI to indicate that an unknow Device is found
     */
    private void displayUnknowDeviceFound(){
        detectionMsg.setText(i18n.getString("detect_lbl_unknownDeviceFound"));
        showTryAgainButton(false);
        displayBuyNewDeviceBtn();
    }
    
    /**
     * update UI to tell that an incompatible device has been found
     * @param model 
     */
    private void displayIncompatibleDeviceFound(String model){
        detectionMsg.setText(String.format(i18n.getString("detect_lbl_incompatibleDeviceFound"), model));
        showTryAgainButton(false);
        displayBuyNewDeviceBtn();
    }
    
    /**
     * update UI to tell how to authorize the device
     */
    private void displayUnauthorizedDeviceFound(){

        VBox container = new VBox();
        container.maxWidthProperty().bind(deviceDetectedRoot.widthProperty().multiply(0.69));
        container.setAlignment(Pos.CENTER);
        container.setSpacing(25.0);
        
        Label lblA = new Label(i18n.getString("detect_lbl_unauthorizedDeviceFound"));
        lblA.setWrapText(true);
        lblA.setTextAlignment(TextAlignment.CENTER);
        
        Label lblB = new Label(i18n.getString("detect_lbl_acceptComputerFingerprint"));
        lblB.setWrapText(true);
        lblB.setTextAlignment(TextAlignment.CENTER);
        
        ImageView imageView = new ImageView(UiUtils.loadImage("rsaFingerprint.png"));
        
        Label lblC = new Label(i18n.getString("detect_lbl_redisplayAllowUsbDebugingMsg"));
        lblC.setWrapText(true);
        lblC.setTextAlignment(TextAlignment.CENTER);
        
        container.getChildren() .addAll( lblA, lblB, imageView, lblC);
        
        deviceDetectedRoot.getChildren().add(0, container);
        UiUtils.hideNode(detectionMsg);
        showTryAgainButton(true);
    }
    
    /**
     * Show a button to lead the user to the shop
     */
    private void displayBuyNewDeviceBtn(){
        UiUtils.showNode(buyNewDevice);
    }
    
    /**
     * Show a button to le the user retry the detection
     */
    private void showTryAgainButton(boolean withSameDevice){
        String instruction;
        if(withSameDevice){
            instruction = "detect_btn_tryAgain";
        }else{
            instruction = "detect_btn_tryWithAnotherDevice";
        }
        startDetectionButton.setText(i18n.getString(instruction));
        UiUtils.showNode(startDetectionButton);
    }

    /**
     * Open /e/ Shop in a browser
     */
    public void goToShop(){
        logger.debug("goToShop()");
        parentController.openUrlInBrowser("https://esolutions.shop/");
    }
}