/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.subcontrollers.AbstractSubController;
import ecorp.easy.installer.EasyInstaller;
import ecorp.easy.installer.controllers.stepControllers.CustomExecutableController;
import ecorp.easy.installer.controllers.stepControllers.CustomStepController;
import ecorp.easy.installer.controllers.stepControllers.StepController;
import ecorp.easy.installer.controllers.stepControllers.Stoppable;
import ecorp.easy.installer.models.Phone;
import ecorp.easy.installer.models.steps.IStep;
import ecorp.easy.installer.tasks.CommandExecutionTask;
import ecorp.easy.installer.tasks.CommandRunnerService;
import ecorp.easy.installer.utils.UiUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.fxml.Initializable;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.control.Button;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Main window Controller class
 *
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class MainWindowController implements Initializable {
    private final static Logger logger = LoggerFactory.getLogger(MainWindowController.class);
    @FXML AnchorPane root;
    @FXML Button nextButton;
    @FXML Label titleLabel;
    
    private ResourceBundle i18n ;//internationalization
    private String currentSubRootId =null;
    Phone phone; //Object encapsulating data about the device to flash
    private String currentStepKey = "f0";
    
    private Object subController;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb The resourceBundle used for internationalization
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        logger.info("initialize()");
        this.i18n = rb;
        disableNextButton(false); //just for test
        loadSubScene();
        DoubleProperty fontSize = new SimpleDoubleProperty(0);
        
        fontSize.bind(root.widthProperty().add(root.heightProperty()).divide(123.2)); //72 = 1440 (default width): 20 (default font size). 51.2 = 1024 / 20        
        root.styleProperty().bind(
            Bindings.concat("-fx-font-size: ", fontSize.asString("%.0f")).concat("px;")
        );
        
        StepController.setParentController(this);
        
    }
    
    /**
     * Open URL of help 
     */
    public void onNeedHelpBtnClick(){ openUrlInBrowser("https://doc.e.foundation/easy-installer-faq"); }
    
    /**
     * open URL of donation
     */
    public void onDonateBtnClick(){  openUrlInBrowser("https://e.foundation/donate/"); }
    
    /**
     * Event happening when using click on "NextButton"
     */
    public void onNextBtnClick(){ loadSubScene(); }
    
    /**
     * Define if nextButton is disable or not
     * @param value if true the button will be disable. If false it won't be.
     */
    synchronized public void disableNextButton(boolean value){
        nextButton.setDisable(value);
    }
        
    /**
     * Set the new text Of NextButton
     * @param newTextKey specify the key that should be use by ResourceBundle
     */
    public void setNextButtonText(String newTextKey){
        nextButton.setText(i18n.getString(newTextKey));
    }
    /**
     * Change the handler for when next button is clicked
     * @param eh the new event Handler
     */
    public void setNextButtonOnClickListener(EventHandler<MouseEvent> eh){
        nextButton.setOnMouseClicked(eh);
    }
    
    /**
     * Hide the Next button
     * @param value true to hide, false either
     */
    synchronized public void setNextButtonVisible(boolean value){
        nextButton.setVisible(value);
    }
    
    /**
     * Reset the handler for when next button is clicked
     */
    public void resetNextButtonEventHandler(){
        logger.info("ResetNextButtonEventHandler()");
        nextButton.setOnMouseClicked((MouseEvent event)->{
            onNextBtnClick();
        });
    }
    
    /**
     * Change title in the currentView
     * @param titleKey key for translation
     */
    public void setViewTitle(String titleKey){
        this.titleLabel.setText(i18n.getString(titleKey));
    }
    
    /**
     * Load different group of controls depending of the current step (of whole process)
     */
    public void loadSubScene(){
        logger.info("loadSubScene("+currentSubRootId+")");
        
        if(currentSubRootId == null || currentSubRootId.isEmpty()){
            
            currentSubRootId =loadSubUI("1-beforeYouBegin.fxml", "before_mTitle", null).getId();
        }else{
            switch(currentSubRootId){
                case "beforeYouBeginRoot":
                    changeView("2-connectDevice.fxml", "connect_mTitle", null);
                    break;
                case "connectDeviceRoot":
                    changeView("3-enableADB.fxml", "devMode_mTitle", null);
                    break;
                case "enableDevMode":
                    if(AppConstants.isWindowsOs()){
                        changeView("3-2-checkDriverInstallation.fxml", "checkDriverInstall_mTitle", null);
                    }else{
                        changeView("4-deviceDetected.fxml", "detect_mTitle", null);
                        disableNextButton(true);
                    }
                    break;
                case "checkDriverInstallation":
                    changeView("4-deviceDetected.fxml", "detect_mTitle", null);
                    disableNextButton(true);                    
                    break;
                case "deviceDetectedRoot":
                    changeView("5-downloadSrc.fxml", "download_mTitle", null);
                    disableNextButton(true);
                    break;
                case "uiRoot":
                case "downloadSceneRoot":
                    StepController controller = null;
                    String fxmlName = "";
                    String title ="installationTitle";
                    if(currentStepKey.equals(IStep.LAST_STEP_KEY)){
                        if(phone.isFlashed()){
                            fxmlName="7-flashResult.fxml";
                            title="installationTitle";
                        }else{
                            fxmlName="9-feedback.fxml";
                            title="feedback_mTitle";
                        }
                    }else{
                        final IStep step = phone.getFlashingProcess().getSteps().get(currentStepKey);

                        switch(step.getType()){
                            case "askAccount":
                                fxmlName = "6-2-eAccount.fxml";
                                break;
                            case "enableOemUnlock":
                                fxmlName = "6-3-unlockOEM.fxml";
                                break;
                            case "custom":
                                controller = new CustomStepController();
                                fxmlName="customStep.fxml";
                                break;
                            case "custom-executable":
                                controller = new CustomExecutableController();
                                fxmlName="customStep.fxml";
                                break;
                            case "executable":
                                //no interface there
                                break;
                            case "load":
                                fxmlName = "loadStep.fxml";
                                break;
                            default:
                                //no interface there
                                break;
                        }
                    }
                    
                    if(!fxmlName.isBlank())
                        changeView(fxmlName, title, controller);
                    else
                        logger.error("No interface to load!");
                    
                    break;
                case "flashResultRoot":
                    final String fxmlFileName;
                    final String nextViewTitle;
                    if( phone.isFlashed() ){
                        fxmlFileName = "8-congrats.fxml";
                        nextViewTitle ="congrats_mTitle";
                    }
                    else{
                        fxmlFileName = "9-feedback.fxml";
                        nextViewTitle ="feedback_mTitle";    
                    }
                    changeView(fxmlFileName, nextViewTitle, null);
                    break;
                case "congratsRoot":
                    changeView("9-feedback.fxml", "feedback_mTitle", null);
                    break;
                case "feedbackRoot":
                    Platform.exit();
                    break;  
                default:
                    logger.error("Invalid currentSubRootId");
                    break;
            }
        }
    }
    
    /**
     * Reset the flashing process to the begining
     */
    public void retryToFlash(){
        currentStepKey = "f0";
        loadSubScene();
    }
    
    /**
     * Put device object inside the factory
     * @param phone the device object
     */
    public void setPhone(Phone phone){
        this.phone = phone;
        CommandExecutionTask.updateCommonParam("DEVICE_ID", phone.getSerialNo());
        CommandExecutionTask.updateCommonParam("DEVICE_DEVICE", phone.getAdbDevice());
    }
    
    /**
     * Used to remove currentView's root node from scene
     * Required for return to flashScene after accountUI view
     * @param rootId 
     */
    public void removeNodeFromRoot(final String rootId) {
        root.getChildren().removeIf( n -> n.getId().equals(rootId));
    }
    
    /**
     * Close previous view after a fadeOut, and open new view
     * @param fxmlName
     * @param viewTitle 
     * @param controller optionnal controller for the new UI. Should be null if already defined in FXML
     */
    public void changeView(final String fxmlName, final String viewTitle, StepController controller){
        logger.debug("change view()");
        
        final String previousNodeId = currentSubRootId;
        Node elementNode = null;
        //get current subroot
        for(Node node : root.getChildren() ){
            if( node.getId().equals(previousNodeId) ){
                elementNode = node;
                break;
            }
        }
        
        if(elementNode == null){
            logger.debug("no subnode found");
            return ;
        }
        //apply transition
        final FadeTransition fadeOutTransition = UiUtils.buildFadeTransition(elementNode, true);
        
        fadeOutTransition.setOnFinished(e -> { 
            removeNodeFromRoot(previousNodeId);
            //logger.debug("Fade out for previous interface finished");
            final Node subRoot =  loadSubUI(fxmlName, viewTitle, controller);
            if (subRoot != null)
            {
                currentSubRootId= subRoot.getId();
                subRoot.setOpacity(0.0);
                UiUtils.buildFadeTransition(subRoot,false).play();
            }
        });
        fadeOutTransition.play();
    }
    
 
    /**
     * Load UI from FXML file
     * @param fxmlName
     * @param controller an optionnal controller for the UI. Cann be null!
     * @return 
     */
    public FXMLLoader loadFXML(String fxmlName, StepController controller){
        FXMLLoader loader;
        loader = new FXMLLoader(getClass().getResource(EasyInstaller.FXML_PATH+fxmlName));
        loader.setResources(i18n);
        
        if(controller != null){
            loader.setController(controller);
            subController = controller;
        }
        
        try{
            loader.load();
        }catch(IOException e){
            logger.error("fxmlName = {}, error = {}", fxmlName, e.toString());
            e.printStackTrace();
            return null;
        }
        if(controller == null){
            final Object ctrl = loader.getController();
            
            if(ctrl != null){
                subController = ctrl;   
                if( ctrl instanceof AbstractSubController ) 
                {
                    ((AbstractSubController)ctrl).setParentController(this);
                }
            }
        }
        return loader;
    }
    /**
     * Load the subScene from FXML and return the controller
     * @param fxmlName fxml file name
     * @param viewTitle the title for the new View
     * @param controller optionnal parameter. It's the controller for the UI if not defined in FXML
     * @return subRoot.
     */
    public Node loadSubUI(final String fxmlName, final String viewTitle, StepController controller){
        if(viewTitle != null) titleLabel.setText(i18n.getString(viewTitle));
        FXMLLoader loader = loadFXML(fxmlName, controller);
        if(loader == null) return null;
        
        Pane subRoot = (Pane) loader.getRoot();
        
        root.getChildren().add(0, subRoot); //adding this element as first subNode, let other element like button to still be clickable on small screen
        
        return subRoot;
    }
    
    /**
     * Show the current UI identified by currentSubRootId
     * It used to get back to FlashScene after eAccount.fxml display
     */
    public void showCurrentSubRoot(){
        this.root.getChildren().forEach(
            ( node)-> { 
                if(node.getId().equals(currentSubRootId)) 
                {
                    UiUtils.showNode(node);
                } 
            });
    }
    
// Other methods
    /**
     * Method call when app is closing
     * It is in charge to close the opened flash or preparation Thread
     */
    public void onStop(){
        logger.debug("onStop()");
        if(subController != null && subController instanceof Stoppable){
            ((Stoppable) subController).stop();
        }
    }
    
    /**
     * Open the specified URL in browser depending on the OS
     * Inspired by 
     * https://stackoverflow.com/questions/5226212/how-to-open-the-default-webbrowser-using-java
     * Another solution can be found here:
     * https://stackoverflow.com/questions/16604341/how-can-i-open-the-default-system-browser-from-a-java-fx-application
     * @param url url to open
     */
    public void openUrlInBrowser(String url){
        final String osName = AppConstants.OsName.toLowerCase();
        Runtime rt = Runtime.getRuntime();
        try{
            if(osName.contains("win")){
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
            }else if(osName.contains("mac")){
                rt.exec("open " + url);
            }else if(osName.contains("nix") || osName.contains("nux")){
                rt.exec("xdg-open "+url); //https://doc.ubuntu-fr.org/xdg-open
            }
            else{
                throw new Exception("OS not supported"); //Not Supported OS
            }
        }catch(Exception e){
            logger.error("url = {}, error = {}", url, e.toString());
        }
    }
    
    /**
     * Allow subController to access to the device's information
     * @return 
     */
    public Phone getPhone() {
        return phone;
    }
    
    
    public String getCurrentStepKey() {
        return currentStepKey;
    }

    
    public void setCurrentStepKey(String currentStepKey) {
        this.currentStepKey = currentStepKey;
    }

}
