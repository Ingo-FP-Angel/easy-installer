/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecorp.easy.installer.graphics;

import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

/**
 * This class encapsulates data and behaviour of an instruction
 * @author vincent
 */
public class Instruction extends TextFlow{
    private final static String HIGHLIGHTED_CSS = "currentSubStep";
    private final String key; //Used for picture
    private final String text;
    private boolean emphasized;
    
    public Instruction(String key,  String text) {
        this.text = text;
        this.key = key;
        emphasized = false;
        formatText();
        getStyleClass().add("instructions");
        this.setTextAlignment(TextAlignment.LEFT);
    }
    
    /**
     * Apply style to make is highlighted
     */
    public void emphasize(){
        getChildren().get(0).getStyleClass().add(HIGHLIGHTED_CSS);
        emphasized = true;
    }
    
    /**
     * Remove the style that make it highlighted
     */
    public void trivialize(){
        getChildren().get(0).getStyleClass().remove(HIGHLIGHTED_CSS);
        emphasized = false;
    }
    
    /**
     * Indicate if this text is emphasized or not
     * @return boolean true if emphasized
     */
    public boolean isEmphasized(){
        return emphasized;
    }
    
    /**
     * Share the text key used to get instruction
     * for translation system. It can be used to load
     * corresponding picture
     * @return String the key
     */
    public String getKey(){
        return key;
    }

    /**
     * Format the text for display
     * Note: This method is thought to evolve
     * to add new feature like detecting URL
     */
    private void formatText() {
        //add basic behaviour first
        Label lbl = new Label(text);
        lbl.setWrapText(true);
        lbl.maxWidthProperty().bind(this.widthProperty().subtract(53.0));
        getChildren().add(lbl);
    }

}


