/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import ecorp.easy.installer.models.Process;

/**
 * Encapsulate data about the device
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class Phone {
    private String internalCode; //Internal code of Easy-installer to identify device's model
    private String serialNo;
    private String adbDevice;
    private String adbModel;
    private String adbProduct;
    //private String androidAPI; //Android api level unused yet
    private boolean flashed = false;
    private boolean adbAuthorized = false;
     //private boolean isReady = false; //tell if preparation process succeed
    
    private Process flashingProcess;

 /**
     * get Device internal Code
     * @return 
     */
    public String getInternalCode() {
        return internalCode;
    }

    /**
     * set Device Internal Code
     * @param internalCode String
     */
    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }
    
    /**
     * Result serial number (from "adb devices -l")
     * @return String
     */
    public String getSerialNo() {
        return serialNo;
    }
    
    /**
     * set serial number from "adb devices -l"
     * @param serialNo String
     */
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
    
    /**
     * Result "device" value from "adb devices -l"
     * @return String
     */
    public String getAdbDevice() {
        return adbDevice;
    }

    /**
     * set "device" value from "adb devices -l"
     * @param adbDevice String
     */
    public void setAdbDevice(String adbDevice) {
        this.adbDevice = adbDevice;
    }
    
    /**
     * Result "model" value from "adb devices -l"
     * @return String
     */
    public String getAdbModel() {
        return adbModel;
    }

    /**
     * set "model" value from "adb devices -l"
     * @param adbModel String
     */
    public void setAdbModel(String adbModel) {
        this.adbModel = adbModel;
    }

    /**
     * Result "product" value from "adb devices -l"
     * @return String
     */
    public String getAdbProduct() {
        return adbProduct;
    }
    /**
     * set "product" value from "adb devices -l"
     * @param adbProduct String
     */
    public void setAdbProduct(String adbProduct) {
        this.adbProduct = adbProduct;
    }

    /**
     * tell if device has been flashed with success
     * @return boolean true if device has been flashed with success
     */
    public boolean isFlashed() {
        return flashed;
    }

    /**
     * tell that device has been flashed with success
    */
    public void setFlashed() {
        this.flashed = true;
    }
    
    /**
     * tell if device authorize computer to communicate through adb
     * @return boolean true if device allow computer to communicate
     */
    public boolean isAdbAuthorized() {
        return adbAuthorized;
    }

    /**
     * set device's adb authorization state 
     * @param adbAuthorized boolean 
     */
    public void setAdbAuthorized(boolean adbAuthorized) {
        this.adbAuthorized = adbAuthorized;
    }
    
    /**
     * Get the flashing process of the phone
     * Can return null if not setted
     * @return 
     */
    public Process getFlashingProcess() {
        return flashingProcess;
    }

    
    /**
     * Set the flashing process of the phone
     * @param flashingProcess 
     */
    public void setFlashingProcess(Process flashingProcess) {
        this.flashingProcess = flashingProcess;
    }
}
