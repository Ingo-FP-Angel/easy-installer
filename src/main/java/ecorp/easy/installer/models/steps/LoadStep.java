/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

import ecorp.easy.installer.models.Command;
import java.util.ArrayList;

/**
 * This encapsulate Step where user have to wait for
 * a background job to be done, and where a ProgressIndicator
 * is present
 * @author vincent Bourgmayer
 */
public class LoadStep extends CustomExecutableStep{
    private int averageTime; //Used to fill the ProgressIndicator in UI

    public LoadStep(String type, String nextStepKey, int stepNumber, String titleKey, String titleIconName, ArrayList<String> contentKeys, Command command, int averageTime) {
        super(type, nextStepKey,  stepNumber, titleKey, titleIconName, contentKeys, command);
        this.averageTime = averageTime;
    }
    
    /**
     * Get the average time of execution for the background task
     * @return int amount in second
     */
    public int getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(int averageTime) {
        this.averageTime = averageTime;
    }
    
    @Override
    public String getType(){
        return "load";
    }
}
