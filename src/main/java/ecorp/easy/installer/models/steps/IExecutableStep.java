/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

import ecorp.easy.installer.models.Command;

/**
 * Represent a step where the computer
 * has to run some command to
 * interact with the phone
 * 
 * Example is: detecting the phone,
 * pushing content on the phone, ...
 * @author vincent Bourgmayer
 */
public interface IExecutableStep extends IStep{
    /**
     * Return the command to execute in background
     * @return Command instance
     */
    public Command getCommand();
}
